module RemindersHelper
  def format_date(date)
    return if date.nil?

    date.strftime("%F %H:%M")
  end
end

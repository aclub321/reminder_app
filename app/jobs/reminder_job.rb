class ReminderJob < ApplicationJob
  queue_as :default

  def perform(reminder)
    ReminderMailer.send_reminder(reminder).deliver_now
  end
end

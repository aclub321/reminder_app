class Reminder < ApplicationRecord
  belongs_to :user

  validates :title, :presence => true
  validates :description, :presence => true
  validates :remind_at, :presence => true

  validate :reminder_after_2_days

  private

  def reminder_after_2_days
    if remind_at.present? && remind_at < 2.days.from_now
      errors.add(:remind_at, "date must be after 2 days")
    end
  end
end

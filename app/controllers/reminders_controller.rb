class RemindersController < ApplicationController
  before_action :authenticate_user!

  # Throw exception if pundit authorize function not used in any action
  after_action :verify_authorized

  before_action :set_reminder, only: [:show, :destroy]

  # GET /reminders
  def index
    @reminders = current_user.reminders

    authorize Reminder 
  end

  # GET /reminders/1
  def show
  end

  # GET /reminders/new
  def new
    @reminder = Reminder.new

    authorize @reminder
  end

  # POST /reminders
  def create
    @reminder = Reminder.new(reminder_params)

    @reminder.user = current_user

    authorize @reminder

    if @reminder.save
      ReminderJob.set(:wait_until => @reminder.remind_at.to_i)
                 .perform_later(@reminder)

      redirect_to @reminder, notice: 'Reminder was successfully created.'
    else
      render :new
    end
  end

  # DELETE /reminders/1
  def destroy
    @reminder.destroy
    redirect_to reminders_url, notice: 'Reminder was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reminder
      @reminder = Reminder.find(params[:id])

      authorize @reminder
    end

    # Only allow a trusted parameter "white list" through.
    def reminder_params
      params.require(:reminder).permit(:title, :description, :remind_at)
    end
end

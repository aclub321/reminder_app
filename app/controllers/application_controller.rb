class ApplicationController < ActionController::Base
  include Pundit

  # Raises exception if we forgot to call authorize in any action except
  # Case 1: If index, new or create action
  # Case 2: If devise controller actions
  after_action :verify_authorized, :except => [:index, :new, :create]
  after_action :verify_authorized, :unless => :devise_controller?

  rescue_from Pundit::NotAuthorizedError, :with => :user_not_authorized

  private

  def user_not_authorized
    flash[:alert] = "You are not authorized to perform this action."

    redirect_to(request.referrer || root_path)
  end
end

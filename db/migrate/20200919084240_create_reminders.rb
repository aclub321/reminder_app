class CreateReminders < ActiveRecord::Migration[6.0]
  def change
    create_table :reminders do |t|
      t.string :tite
      t.text :description
      t.datetime :remind_at
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end

class RenameColumnTiteInReminder < ActiveRecord::Migration[6.0]
  def change
    rename_column :reminders, :tite, :title
  end
end

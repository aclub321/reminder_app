# README

Through reminder application, a user is able to set reminders. On reaching reminder date and time, an reminder email is sent to the user.


![Screenshot](https://bitbucket.org/aclub321/reminder_app/raw/master/doc/set_reminder.png)

## Reminder limitation
Currently, user can only set reminder date after 2 days. This logic can be changed depending on customer needs. 

## Configuration
* Ruby version
2.7.3
* Rails version
6.0.3.3

## Project Setup

1. bundle install
2. yarn install --check-files
3. rails db:migrate
4. rails db:seed

## How to run reminder application
1. Run `rails s` at command prompt
2. Enter `localhost:3000` on browser
3. Either register a new user or use default user(details given in next section) to login

## Default User
**Email:** john@abc.com

**Password:** 123456

## How to run the test suite
### To run unit tests
rake

### To run system test
rake test:system

## Note
I tried to create a minimum viable product to set and send reminders to users. Few basic test cases are added in this project.



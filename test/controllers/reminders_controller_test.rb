require 'test_helper'

class RemindersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  include ActiveJob::TestHelper

  setup do
    @reminder = reminders(:one)
    
    sign_in users(:one)
  end

  def teardown
    sign_out users(:one)
  end

  test "should get index" do
    get reminders_url
    assert_response :success
  end

  test "should get new" do
    get new_reminder_url
    assert_response :success
  end

  test "should create reminder" do
    assert_difference('Reminder.count') do
      post reminders_url,
           params: {
             reminder: {
               title: @reminder.title,
               description: @reminder.description,
               remind_at: 3.days.from_now
             }
           }
    end

    # Enqeued with 1 ReminderJob
    assert_enqueued_with(:job => ReminderJob)
    assert_enqueued_jobs(1)  

    assert_redirected_to reminder_url(Reminder.last)
  end

  test "should show reminder" do
    get reminder_url(@reminder)
    assert_response :success
  end

  test "user cannot view other users reminder" do
    sign_out users(:one)

    sign_in users(:two)

    get reminder_url(@reminder)

    assert_redirected_to root_url

    assert_equal "You are not authorized to perform this action.", flash[:alert]

    sign_out users(:two)
  end

  test "should destroy reminder" do
    assert_difference('Reminder.count', -1) do
      delete reminder_url(@reminder)
    end

    assert_redirected_to reminders_url
  end

  test "should not get index if user not logged in" do
    sign_out users(:one)

    get reminders_url

    assert_redirected_to new_user_session_url
  end

  test "should not get new if user not logged in" do
    sign_out users(:one)

    get new_reminder_url

    assert_redirected_to new_user_session_url
  end

  test "should not show reminder if user not logged in" do
    sign_out users(:one)

    get reminder_url(@reminder)

    assert_redirected_to new_user_session_url
  end
end

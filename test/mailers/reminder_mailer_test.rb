require 'test_helper'

class ReminderMailerTest < ActionMailer::TestCase
  test "send_reminder" do
    reminder = reminders(:one)

    # Create the email and store it for further assertions
    email = ReminderMailer.send_reminder(reminder)
 
    # Send the email, then test that it got queued
    assert_emails 1 do
      email.deliver_now
    end
 
    # Test the body of the sent email contains what we expect it to
    assert_equal ['from@example.com'], email.from
    assert_equal ['user1@test.de'], email.to
    assert_equal 'Reminder1 (User 1)', email.subject

    assert_equal read_fixture('reminder').join, email.text_part.body.to_s
  end
end

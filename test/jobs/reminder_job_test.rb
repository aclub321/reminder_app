require 'test_helper'

class ReminderJobTest < ActiveJob::TestCase
  test "the truth" do
    reminder = reminders(:one)

    ReminderJob.perform_now(reminder)

    email = ActionMailer::Base.deliveries.last

    # Test if email from, to and subject content correct
    assert_equal ['from@example.com'], email.from
    assert_equal ['user1@test.de'], email.to
    assert_equal 'Reminder1 (User 1)', email.subject
  end
end

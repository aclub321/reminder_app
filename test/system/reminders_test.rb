require "application_system_test_case"

class RemindersTest < ApplicationSystemTestCase
  setup do
    @reminder = reminders(:one)
    
    user = users(:one)
    login_user(user)
  end

  def teardown
    logout_user
  end

  def login_user(user)
    # visit login page
    visit new_user_session_url
    assert_selector "h2", :text => "Log in"

    # Login as Test User
    fill_in 'Email', :with => user.email
    fill_in 'Password', :with => '12345678'
    click_button 'Log in'
  end

  def logout_user
    click_link 'Logout'

    assert_selector '#alert', 
                    :text => 'You need to sign in or sign up before continuing.'
  end

  test "visiting the index" do
    visit reminders_url
    assert_selector "h1", text: "Listing reminders"
  end

  test "creating a Reminder" do
    visit reminders_url
    click_on "New Reminder"

    fill_in "Title", with: "Reminder Title"
    fill_in "Description", with: "This is reminder description"
    page.execute_script("$('#reminder_remind_at').val('#{3.days.from_now}')")
    click_on "Save"

    assert_text "Reminder was successfully created"
    click_on "Back"
  end

  test "destroying a Reminder" do
    visit reminders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Reminder was successfully destroyed"
  end

  test "list all user1 reminders when logged in as user1" do
    visit reminders_url

    assert_selector "h1", text: "Listing reminders"

    within 'table tbody' do
      assert_selector 'tr', :count => 2

      assert_selector 'td', :text => 'Reminder1 (User 1)'
      assert_selector 'td', :text => 'Reminder2 (User 1)'
    end
  end

  test "list all user2 reminders when logged in as user2" do
    user1 = users(:one)
    logout_user

    user2 = users(:two)
    login_user(user2)

    visit reminders_url

    assert_selector "h1", text: "Listing reminders"

    within 'table tbody' do
      assert_selector 'tr', :count => 2

      assert_selector 'td', :text => 'Reminder1 (User 2)'
      assert_selector 'td', :text => 'Reminder2 (User 2)'
    end
  end
end

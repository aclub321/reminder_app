require 'test_helper'

class ReminderTest < ActiveSupport::TestCase
  test "create reminder if all fields present" do
    user = users(:one)
  
    reminder = Reminder.new(:title => 'Test title', 
                            :description => 'This is reminder desciption', 
                            :remind_at => 3.days.from_now,
                            :user_id => user.id)

    assert reminder.save
  end

  test "donot create reminder if description missing" do
    user = users(:one)
  
    reminder = Reminder.new(:title => 'Test title', 
                            :remind_at => 3.days.from_now,
                            :user_id => user.id)

    assert_not reminder.save, "Reminder cannot be created without description"
  end

  test "donot create reminder if remind_at before 2 days" do
    user = users(:one)
  
    reminder = Reminder.new(:title => 'Test title', 
                            :description => 'This is reminder desciption', 
                            :remind_at => 1.day.from_now,
                            :user_id => user.id)

    assert_not reminder.save, 
               "Reminder cannot be created if remind_at date before 2 days"
  end
end

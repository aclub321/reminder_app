require "test_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, :using => :headless_chrome, screen_size: [1400, 1400]
  Capybara.javascript_driver = :selenium_chrome_headless

  # 'take_failed_screenshot' function overridden in order to disable generating
  # screenshots. Disable this function to allow generating screenshots
  def take_failed_screenshot
    false
  end
end
